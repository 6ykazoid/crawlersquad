﻿using System;
using UnityEngine;

public class HotkeyPanel : MonoBehaviour 
{
    private Inventory inventory;
    private WeaponButton[] weponButtons;

    private void Awake()
    {
        weponButtons = GetComponentsInChildren<WeaponButton>();
    }

    private void Start()
    {
        BindToInventory(Player.Instance.GetComponent<Inventory>());
    }

    private void BindToInventory(Inventory inventoryToBind)
    {
        if (inventory != null) inventoryToBind.OnInventoryChanged -= Refresh;        

        inventory = inventoryToBind;
        inventoryToBind.OnInventoryChanged += Refresh;

        Refresh();
    }
    
    private void Refresh()
    {
        var items = inventory.GetItems();
        for (int i = 0; i < items.Count; i++)
        {
            weponButtons[i].SetWeapon(items[i]);
        }
    }

    public void TryEquip(int index)
    {
        bool equipSuccessfull = inventory.EquipItem(index);
    }
}
