﻿using UnityEngine;
using UnityEngine.UI;

public class WeaponButton : MonoBehaviour 
{
    [SerializeField]
    private Image sprite;
    [SerializeField]
    private Text hotkeyText;

    private Button button;
    private int siblingIndex;
    private KeyCode hotkey;
    private HotkeyPanel hotkeyPanel;

    private void Awake()
    {
        hotkeyPanel = GetComponentInParent<HotkeyPanel>();
        button = GetComponent<Button>();
        siblingIndex = transform.GetSiblingIndex();
        hotkey = KeyCode.Alpha1 + siblingIndex;
        hotkeyText.text = (siblingIndex + 1).ToString();

        button.onClick.AddListener(EquipWeapon);
    }

    private void Update()
    {
        if (Input.GetKeyDown(hotkey))
        {
            EquipWeapon();
        }
    }

    public void SetWeapon(Weapon weapon)
    {
        sprite.sprite = weapon.Sprite;
    }

    private void EquipWeapon()
    {
        Debug.Log("Equip Weapon - " + hotkeyText.text);
        hotkeyPanel.TryEquip(siblingIndex);
    }
}
