﻿using UnityEngine;

public class TargetingCircle : MonoBehaviour 
{
    [SerializeField]
    private Material targetMaterial;
    [SerializeField]
    private Material moveMaterial;

    private Renderer quadRenderer;
    private Transform targetTransform;

    public static TargetingCircle Instance { get; private set; }

    private void Awake()
    {
        Instance = this;
        quadRenderer = GetComponentInChildren<Renderer>();
    }

    private void Update()
    {
        if(targetTransform != null && targetTransform as UnityEngine.Object != null)
        {
            transform.position = targetTransform.position;
        }
    }

    public void SetTarget(ITargetable target)
    {
        this.targetTransform = target.transform;
        quadRenderer.material = targetMaterial;
        //transform.SetParent(target.transform);
        //transform.localPosition = Vector3.zero;
    }

    public void SetDestination(Vector3 destination)
    {
        targetTransform = null;
        quadRenderer.material = moveMaterial;
        //transform.SetParent(null);
        transform.position = destination;
    }
}
