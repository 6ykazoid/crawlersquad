﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CursorController : MonoBehaviour 
{
    [SerializeField]
    private Texture2D defaultCursor;
    [SerializeField]
    private Texture2D attackCursor;
    [SerializeField]
    private Texture2D interactCursor;

    [SerializeField]
    private LayerMask layerMask;

    private void Awake()
    {
        Cursor.SetCursor(defaultCursor, Vector3.zero, CursorMode.Auto);
    }

    private void Update()
    {
        if (EventSystem.current.IsPointerOverGameObject())
        {
            Cursor.SetCursor(defaultCursor, Vector3.zero, CursorMode.Auto);
            return;
        }

        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
        {
            ITargetable targetable = hit.collider.GetComponent<ITargetable>();
            if (targetable != null)
            {
                Cursor.SetCursor(attackCursor, Vector3.zero, CursorMode.Auto);

                if(targetable is IInteractable)
                {
                    Cursor.SetCursor(interactCursor, Vector3.zero, CursorMode.Auto);
                }
            }
            else
            {
                Cursor.SetCursor(defaultCursor, Vector3.zero, CursorMode.Auto);
            }
        }
    }
}
