﻿using UnityEngine;
using TMPro;
using System.Collections.Generic;
using System;
using System.Collections;

public class HealthChangeText : MonoBehaviour 
{
    [SerializeField]
    float floatSpeed = 100f;
    [SerializeField]
    float floatTime = 1f;
    [SerializeField]
    float verticalOffset = 3;

    private Health health;
    private TextMeshProUGUI[] textChildren;
    private Queue<TextMeshProUGUI> textQueue;

    private void Awake()
    {
        health = GetComponentInParent<Health>();
        health.OnHealthChanged += Health_OnHealthChanged;

        InitializeText();
    }

    private void InitializeText()
    {
        textChildren = GetComponentsInChildren<TextMeshProUGUI>();
        textQueue = new Queue<TextMeshProUGUI>(textChildren);
        for (int i = 0; i < textChildren.Length; i++)
        {
            textChildren[i].gameObject.SetActive(false);
        }
    }

    private void Health_OnHealthChanged(Health.HealthChangeData data)
    {
        TextMeshProUGUI textToUse = textQueue.Dequeue();

        textToUse.text = data.delta.ToString();
        textToUse.gameObject.SetActive(true);
        textToUse.StartCoroutine(FloatAway(textToUse));
    }

    private IEnumerator FloatAway(TextMeshProUGUI textToUse)
    {
        MoveTextToCorrectPosition(textToUse);

        float elapsed = 0;
        while(elapsed < floatTime)
        {
            textToUse.transform.position += Vector3.up * Time.deltaTime * floatSpeed;
            elapsed += Time.deltaTime;
            yield return null;
        }
        textToUse.gameObject.SetActive(false);
        textToUse.transform.localPosition = Vector3.zero;
        textQueue.Enqueue(textToUse);
    }

    private void MoveTextToCorrectPosition(TextMeshProUGUI textToUse)
    {
        Vector3 worldPosition = health.transform.position + Vector3.up * verticalOffset;
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(worldPosition);
        transform.position = screenPosition;
    }
}
