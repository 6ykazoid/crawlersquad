﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HPBar : MonoBehaviour 
{
    private Health health;
    private Slider slider;
    private Animator animator;
    private bool isHoverOver = true;
    private float stayVisibleUntilTime;
    private bool visible;

    [SerializeField]
    private float verticalOffset = 3;
    [SerializeField]
    private float visibleTime = 3;

    private void Awake()
    {
        slider = GetComponent<Slider>();
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        health = GetComponentInParent<Health>();
        if (health == null)
        {
            health = Player.Instance.GetComponent<Health>();
            isHoverOver = false;
        }

        health.OnHealthChanged += Health_OnHealthChanged;
    }

    private void Update()
    {
        if (isHoverOver) HoverHealthBar();
    }

    private void HoverHealthBar()
    {
        Vector3 worldPosition = health.transform.position + Vector3.up * verticalOffset;
        Vector3 screenPosition = Camera.main.WorldToScreenPoint(worldPosition);
        transform.position = screenPosition;
    }

    private void Health_OnHealthChanged(Health.HealthChangeData data)
    {
        slider.value = data.pct;
        if (animator != null)
        {
            stayVisibleUntilTime = Time.time + visibleTime;
            if (!visible) StartCoroutine(ShowHPBar());
        }
    }

    private IEnumerator ShowHPBar()
    {
        visible = true;
        animator.SetBool("Visible", true);
        while (Time.time < stayVisibleUntilTime)
        {
            yield return null;
        }
        animator.SetBool("Visible", false);
        visible = false;
    }
}
