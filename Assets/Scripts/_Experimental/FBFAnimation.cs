﻿using UnityEngine;

[CreateAssetMenu(menuName = "Experimental/Frame By Frame Animation")]
public class FBFAnimation : ScriptableObject 
{
    [SerializeField]
    private float stepDelay = 0.12f;

    [SerializeField]
    private Mesh[] frames;

    private float nextFrameTime;

    int index;
    bool isPlaying = false;

    private void OnEnable()
    {
        index = 0;
        isPlaying = false;
        nextFrameTime = Time.time + stepDelay;
    }

    public void Play(MeshFilter meshFilter)
    {
        if (isPlaying)
        {
            index = index % frames.Length;
            if (Time.time - nextFrameTime > stepDelay)
            {
                meshFilter.sharedMesh = frames[index];
                nextFrameTime = Time.time + stepDelay;
                index++;
            }
        }
        else
        {
            isPlaying = true;
            index = 0;
        }
    }
}
