﻿using UnityEngine;

public class FBFAnimationController : MonoBehaviour 
{
    [SerializeField]
    private FBFAnimation animation;

    private MeshFilter meshFilter;

    private void Awake()
    {
        meshFilter = GetComponentInChildren<MeshFilter>();
    }

    private void Update()
    {
        animation.Play(meshFilter);
    }
}
