﻿using UnityEngine;

[CreateAssetMenu(menuName = "Audio/Simple Event")]
public class SimpleAudioEvent : ScriptableObject
{
    [SerializeField]
    private AudioClip[] audioClips;
    [SerializeField]
    private RangedFloat volume = new RangedFloat(0.5f, 1f);

    public void Play(AudioSource audioSource)
    {
        audioSource.volume = volume.RandomInRange();
        AudioClip selectedClip = RandomClip();
        audioSource.PlayOneShot(selectedClip);
    }

    private AudioClip RandomClip()
    {
        int index = UnityEngine.Random.Range(0, audioClips.Length);
        return audioClips[index];
    }
}
