﻿using System;
using UnityEngine;

[RequireComponent(typeof (AudioSource))]
public class CharacterAudio : MonoBehaviour 
{
    [SerializeField]
    private SimpleAudioEvent idleSound;
    [SerializeField]
    private SimpleAudioEvent impactSound;
    [SerializeField]
    private SimpleAudioEvent deathSound;
    [SerializeField]
    [MinMaxRange(3, 20)]
    private RangedFloat idleSoundDelay;

    private Health health;
    private AudioSource audioSource;
    private float nextIdleSoundTime;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        health = GetComponent<Health>();
        if(health != null)
        {
            health.OnTakeDamage += Health_OnTakeDamage;
            health.OnDeath += Health_OnDeath;
        }
    }

    private void Health_OnDeath()
    {
        if (deathSound != null)
        {
            deathSound.Play(audioSource); 
        }
        this.enabled = false;
    }

    private void Health_OnTakeDamage()
    {
        if (impactSound != null)
        {
            impactSound.Play(audioSource);
        }
    }

    private void Update()
    {
        if (ShouldPlayIdleSound())
        {
            PlayIdleSound();
        }
    }

    private bool ShouldPlayIdleSound()
    {
        return Time.time > nextIdleSoundTime;
    }

    private void PlayIdleSound()
    {
        nextIdleSoundTime = Time.time + idleSoundDelay.RandomInRange();
        idleSound.Play(audioSource);
    }
}
