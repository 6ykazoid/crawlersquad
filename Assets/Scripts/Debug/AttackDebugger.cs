﻿using System;
using System.Collections;
using UnityEngine;

public class AttackDebugger : MonoBehaviour 
{
    private Renderer meshrenderer;

    private void Awake()
    {
        meshrenderer = GetComponent<Renderer>();
        GetComponent<Weapon>().OnAttack += AttackDebugger_OnAttack;
    }

    private void AttackDebugger_OnAttack()
    {
        StartCoroutine(FlashRed());
    }

    private IEnumerator FlashRed()
    {
        Color temp = meshrenderer.material.color;
        meshrenderer.material.color = Color.red;
        yield return new WaitForSeconds(0.25f);
        meshrenderer.material.color = temp;
    }

    private void OnDestroy()
    {
        GetComponent<Weapon>().OnAttack -= AttackDebugger_OnAttack;
    }
}
