﻿using System;
using UnityEngine;

public class Projectile : PooledMonoBehaviour
{
    [SerializeField]
    private float speed;
    [SerializeField]
    private float hitRange;
    [SerializeField]
    private int damage;

    private IAttackable target;

    public void FireAt(IAttackable target)
    {
        this.target = target;
    }

    private void Update()
    {
        if (target.IsInvalid())
        {
            Die();
        }
        else if (HitTarget())
        {
            DamageTarget();
            Die();
        }
        else
        {
            MoveTowardstarget();
        }
    }

    private void Die()
    {
        gameObject.SetActive(false);
    }

    private void MoveTowardstarget()
    {
        Vector3 directionToTarget = Vector3.Normalize(target.transform.position - transform.position);
        transform.position += directionToTarget * Time.deltaTime * speed;
    }

    private void DamageTarget()
    {
        target.TakeDamage(damage);
    }

    private bool HitTarget()
    {
        return Vector3.Distance(transform.position, target.transform.position) < hitRange;
    }
}