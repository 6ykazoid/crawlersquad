﻿using System;
using System.Collections;
using UnityEngine;

public abstract class Weapon : MonoBehaviour
{
    [SerializeField]
    protected int damage = 1;
    [SerializeField]
    protected float attackDelay = 1f;
    [SerializeField]
    protected float range = 5f;
    public float Range { get { return range; } }
    [SerializeField]
    private Sprite sprite;
    public Sprite Sprite { get { return sprite; } }

    private float lastAttackTime;

    protected abstract IEnumerator DoAttack(IAttackable target);

    public event Action OnAttack = delegate { };

    public void AttackTarget(IAttackable target)
    {
        OnAttack();
        lastAttackTime = Time.time;
        StartCoroutine(DoAttack(target));
    }

    public bool WeaponIsReady()
    {
        return Time.time - lastAttackTime > attackDelay;
    }
}
