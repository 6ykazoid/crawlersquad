﻿using System.Collections;
using UnityEngine;

public class MeleeWeapon : Weapon
{
    [SerializeField]
    private float delayBeforeHit = 0.2f;

    protected override IEnumerator DoAttack(IAttackable target)
    {
        yield return new WaitForSeconds(delayBeforeHit);
        target.gameObject.GetComponent<Health>().TakeDamage(damage);
    }

    private void OnValidate()
    {
        if (delayBeforeHit > attackDelay && attackDelay > 0)
        {
            delayBeforeHit = attackDelay - 0.1f;
            delayBeforeHit = Mathf.Max(delayBeforeHit, 0f);
        }
    }
}
