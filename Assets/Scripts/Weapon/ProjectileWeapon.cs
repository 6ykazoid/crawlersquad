﻿using System.Collections;
using UnityEngine;

public class ProjectileWeapon : Weapon
{
    [SerializeField]
    private Projectile projectilePrefab;

    [SerializeField]
    private Transform firePoint;

    [SerializeField]
    private float castTime = 1.2f;

    protected override IEnumerator DoAttack(IAttackable target)
    {
        yield return new WaitForSeconds(castTime);
        Projectile projectile = projectilePrefab.Get<Projectile>(firePoint.position, firePoint.rotation);
        projectile.FireAt(target);
    }
}
