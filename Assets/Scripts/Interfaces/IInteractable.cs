﻿public interface IInteractable : ITargetable
{
    float InteractRange { get; }
    void Interact(Brain brain);
}