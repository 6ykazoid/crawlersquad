﻿using UnityEngine;

public static class ITargetableExtensions
{
    public static bool IsInvalid(this ITargetable targetable)
    {
        return targetable == null || (targetable as UnityEngine.Object) == null || targetable.CanInteract() == false;
    }
}