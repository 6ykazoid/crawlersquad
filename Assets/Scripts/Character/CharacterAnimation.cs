﻿using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class CharacterAnimation : MonoBehaviour 
{
    private NavMeshAgent agent;
    private Animator animator;
    private Brain brain;
    private Health health;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponentInChildren<Animator>();
        brain = GetComponent<Brain>();
        brain.OnAttackStarted += HandleAttackStarted;
        health = GetComponent<Health>();

        if(health != null)
        {
            health.OnDeath += Health_OnDeath;
        }
    }

    private void Health_OnDeath()
    {
        animator.SetTrigger("Death");
    }

    private void HandleAttackStarted(Weapon weapon)
    {
        if (weapon is MeleeWeapon) animator.SetTrigger("Attack");
        if (weapon is ProjectileWeapon) animator.SetTrigger("Cast");
    }

    private void Update()
    {
        float speed = agent.velocity.magnitude;
        animator.SetFloat("Speed", speed);
    }
}
