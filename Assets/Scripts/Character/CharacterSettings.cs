﻿using UnityEngine;

public class CharacterSettings : MonoBehaviour
{
    [SerializeField]
    private float agroRange = 5f;
    public float AgroRange { get { return agroRange; } }

    [SerializeField]
    private float stopChaseRange = 10;
    public float StopChaseRange { get { return stopChaseRange; } }

    [SerializeField]
    private float wanderDistance = 0;
    public float WanderDistance { get { return wanderDistance; } }
}
