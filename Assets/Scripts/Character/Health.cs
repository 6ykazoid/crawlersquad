﻿using System;
using UnityEngine;

public class Health : MonoBehaviour, IAttackable
{
    [SerializeField]
    private int maxHealth = 5;
    [SerializeField]
    private float deathCleanupDelay = 3;

    private int health;

    public int HP { get { return health; } }

    public event Action OnTakeDamage = delegate { };
    public event Action<HealthChangeData> OnHealthChanged = delegate { };
    public event Action OnDeath = delegate { };

    public struct HealthChangeData
    {
        public int current;
        public int max;
        public int delta;
        public float pct;
    }

    private void Awake()
    {
        health = maxHealth;
    }

    public void TakeDamage(int damage)
    {
        health -= damage;

        OnTakeDamage();
        SendHealthChange(damage);

        if (health <= 0) Die();
    }

    private void SendHealthChange(int damage)
    {
        HealthChangeData data = new HealthChangeData()
        {
            current = health,
            max = maxHealth,
            delta = damage,
            pct = (float)health / (float)maxHealth
    };

        OnHealthChanged(data);
    }

    private void Die()
    {
        OnDeath();
        Destroy(gameObject, deathCleanupDelay);
        this.enabled = false;
    }

    public bool CanInteract()
    {
        return health > 0;
    }
}
