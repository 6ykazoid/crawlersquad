﻿using System;
using UnityEngine;

public class ToggleOutline : MonoBehaviour 
{
    cakeslice.Outline outline;

    [SerializeField]
    private float raycastRadius = 2f;
    //[SerializeField]
    //private LayerMask layerMask;

    private void Awake()
    {
        outline = GetComponentInChildren<cakeslice.Outline>();
    }

    private void Update()
    {
        if (PlayerIsVisible())
        {
            outline.enabled = false;
        }
        else
        {
            outline.enabled = true;
        }
    }

    private bool PlayerIsVisible()
    {
        Vector3 cameraPosition = Camera.main.transform.position;
        Vector3 direction = transform.position - cameraPosition;

        Ray ray = new Ray(cameraPosition, direction);
        RaycastHit hitInfo;

        if(Physics.SphereCast(ray, raycastRadius, out hitInfo, Mathf.Infinity))
        {
            if(hitInfo.collider.gameObject == this.gameObject)
            {
                return true;
            }
        }

        return false;
    }
}
