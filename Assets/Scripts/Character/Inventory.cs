﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour 
{
    [SerializeField]
    private Weapon defaultWeaponPrefab;
    [SerializeField]
    private Transform weaponLocation;

    private List<Weapon> items = new List<Weapon>();
    private Transform inventoryRootTransform;

    public event Action OnInventoryChanged = delegate { };

    public Weapon CurrentWeapon { get; private set; }

    public List<Weapon> GetItems()
    {
        return new List<Weapon>(items);
    }

    private void Awake()
    {
        inventoryRootTransform = new GameObject("Inventory Root").transform;
        inventoryRootTransform.SetParent(transform);
        inventoryRootTransform.localPosition = Vector3.zero;
        AddDefaultWeapon(defaultWeaponPrefab);
    }

    private void AddDefaultWeapon(Weapon defaultWeaponPrefab)
    {
        if(defaultWeaponPrefab != null)
        {
            Weapon defaultWeapon = Instantiate(defaultWeaponPrefab);
            AddItem(defaultWeapon);
            Equip(defaultWeapon);
        }
    }

    public void AddItem(Weapon item)
    {
        items.Add(item);
        item.transform.SetParent(inventoryRootTransform);
        item.transform.localPosition = Vector3.zero;
        item.gameObject.SetActive(false);

        OnInventoryChanged();
    }

    private void Equip(Weapon weapon)
    {
        if (CurrentWeapon != null) UnEquip();

        CurrentWeapon = weapon;
        CurrentWeapon.transform.SetParent(weaponLocation);
        CurrentWeapon.transform.localPosition = Vector3.zero;
        CurrentWeapon.transform.localRotation = Quaternion.identity;
        CurrentWeapon.gameObject.SetActive(true);
    }

    private void UnEquip()
    {
        CurrentWeapon.transform.SetParent(inventoryRootTransform);
        CurrentWeapon.gameObject.SetActive(false);
    }

    public bool EquipItem(int index)
    {
        if (items.Count > index)
        {
            Equip(items[index]);
            return true;
        }
        return false;
    }
}
