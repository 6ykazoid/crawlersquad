﻿using UnityEngine;
using UnityEngine.AI;

public abstract class CharacterState
{
    protected Brain brain;
    protected Transform transform;
    protected NavMeshAgent navmeshagent;
    protected CharacterSettings settings;
    protected Inventory inventory;

    protected Weapon Weapon { get { return inventory.CurrentWeapon; } }

    public CharacterState(Brain brain)
    {
        this.brain = brain;
        this.transform = brain.transform;
        this.navmeshagent = brain.Navmeshagent;
        this.settings = brain.GetComponent<CharacterSettings>();
        this.inventory = brain.GetComponent<Inventory>();
    }

    public CharacterState NextState { get; set; }

    

    public abstract void EnterState();
    public abstract void ExitState();
    public abstract void Tick();

}