﻿using System;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(CharacterSettings))]
public class Brain : MonoBehaviour
{
    private CharacterState currentState;

    public Action<Weapon> OnAttackStarted = delegate { };

    public NavMeshAgent Navmeshagent { get; private set; }

    public virtual CharacterState DefaultState()
    {
        return new WaitForAggro(this);
    }

    private void Awake()
    {
        Navmeshagent = GetComponent<NavMeshAgent>();
        GetComponent<Health>().OnDeath += Brain_OnDeath;
        SwitchState(DefaultState());
    }

    private void Brain_OnDeath()
    {
        Navmeshagent.enabled = false;
        this.enabled = false;
    }

    private void Update()
    {
        if (currentState != null)
        {
            currentState.Tick();

            if (currentState.NextState != null)
            {
                SwitchState(currentState.NextState);
            }
        }
    }

    protected void SwitchState(CharacterState newState)
    {
        if (currentState != null) currentState.ExitState();

        currentState = newState;
        currentState.EnterState();
    }

    public override string ToString()
    {
        string outputString = string.Format(GetComponent<Health>().HP + "hp" + Environment.NewLine);
        if (currentState != null) return outputString + currentState.ToString();

        return outputString + "NO STATE";
    }
}
