﻿using UnityEngine;

public class MoveToPosition : CharacterState
{
    private Vector3 destination;

    public MoveToPosition(Brain brain, Vector3 destination) : base(brain)
    {
        this.destination = destination;
    }

    public override void EnterState()
    {
        navmeshagent.SetDestination(destination);
    }

    public override void ExitState()
    {
    }

    public override void Tick()
    {
        if (navmeshagent.remainingDistance <= 0) NextState = new Idle(brain);
    }

    public override string ToString()
    {
        return string.Format("MoveToPosition");
    }
}
