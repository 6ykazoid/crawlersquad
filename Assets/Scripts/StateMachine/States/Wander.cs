﻿using System;
using UnityEngine;

public class Wander : CharacterState
{
    private Vector3 startPosition;

    public Wander(Brain brain) : base(brain)
    {
    }

    public override void EnterState()
    {
        startPosition = brain.transform.position;
    }

    public override void ExitState()
    {
    }

    public override void Tick()
    {
        if (DestinationNeeded())
        {
            ChooseRandomdestination();
        }
    }

    private void ChooseRandomdestination()
    {
        Vector3 randomOffset = Vector3Extensions.Random(-settings.WanderDistance, settings.WanderDistance);
        randomOffset = randomOffset.With(y: settings.WanderDistance * 2);

        Vector3 randomDestination = startPosition + randomOffset;

        int layer = LayerMask.GetMask("Ground");
        float distance = settings.WanderDistance * 3;

        Debug.DrawRay(randomDestination, Vector3.down * distance, Color.green, 5f);

        RaycastHit hitInfo;
        if (Physics.Raycast(randomDestination, Vector3.down, out hitInfo, distance, layer))
        {
            navmeshagent.SetDestination(hitInfo.point);
        }
    }

    private bool DestinationNeeded()
    {
        return navmeshagent.desiredVelocity.sqrMagnitude <= 0;
    }
}