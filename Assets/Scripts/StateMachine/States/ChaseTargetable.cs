﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class ChaseTargetable : CharacterState
{
    private float distance;
    private ITargetable target;

    public ChaseTargetable(Brain brain, ITargetable target) : base(brain)
    {
        this.target = target;
    }

    public override void EnterState()
    {
        
    }
    
    public override void ExitState()
    {
        navmeshagent.SetDestination(brain.transform.position);
    }

    public override void Tick()
    {
        if (target.IsInvalid())
        {
            NextState = brain.DefaultState();
            return;
        }

        distance = Vector3.Distance(target.transform.position, brain.transform.position);
        navmeshagent.SetDestination(target.transform.position);

        if (target is IAttackable)
        {
            Attack();
        }
        if (target is IInteractable)
        {
            Interact();
        }
    }

    private void Attack()
    {
        if (distance > settings.StopChaseRange)
        {
            NextState = new WaitForAggro(brain);
        }
        else if(distance <= Weapon.Range)
        {
            NextState = new AttackTarget(brain, target as IAttackable);
        }
    }

    private void Interact()
    {
        IInteractable interactable = target as IInteractable;
        if (interactable.CanInteract() && distance <= interactable.InteractRange)
        {
            interactable.Interact(brain);
            NextState = brain.DefaultState();
        }
    }

    public override string ToString()
    {
        return string.Format("ChaseTargetable {0:0.0}/{1}", distance, settings.StopChaseRange);
    }
}
