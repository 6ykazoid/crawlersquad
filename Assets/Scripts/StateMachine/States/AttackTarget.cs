﻿using System;
using UnityEngine;

internal class AttackTarget : CharacterState
{
    private IAttackable target;
    
    public AttackTarget(Brain brain, IAttackable target) : base(brain)
    {
        this.target = target;
    }
    
    public override void EnterState()
    {
    }

    public override void ExitState()
    {
    }

    public override void Tick()
    {
        if (target.IsInvalid())
        {
            NextState = brain.DefaultState();
            return;
        }

        float distance = Vector3.Distance(brain.transform.position, target.transform.position);

        if (distance > Weapon.Range)
        {
            NextState = new ChaseTargetable(brain, target);
        }
        else
        {
            TryAttack();
        }
    }

    private void TryAttack()
    {
        if (Weapon.WeaponIsReady())
        {
            Weapon.AttackTarget(target);
            brain.OnAttackStarted(Weapon);
        }
    }
}