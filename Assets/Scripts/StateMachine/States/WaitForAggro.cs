﻿using UnityEngine;

public class WaitForAggro : CharacterState
{
    private float distance;

    public WaitForAggro(Brain brain) : base(brain) { }

    public override void EnterState()
    {

    }

    public override void ExitState()
    {

    }

    public override void Tick()
    {
        if (PlayerInRange()) NextState = new ChaseTargetable(brain, Player.Instance.Health);
    }

    private bool PlayerInRange()
    {
        var player = GameObject.FindObjectOfType<Player>();
        distance = Vector3.Distance(brain.transform.position, player.transform.position);

        return distance < settings.AgroRange;
    }

    public override string ToString()
    {
        return string.Format("WaitForAggro {0:0.0}/{1}", distance, settings.AgroRange);
    }
}