﻿using UnityEngine;

public class PlayerBrain : Brain
{
    public override CharacterState DefaultState()
    {
        return new Idle(this);
    }

    public void TrySwitchState(CharacterState state)
    {
        base.SwitchState(state);
    }
}

