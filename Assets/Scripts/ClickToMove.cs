﻿using UnityEngine;
using UnityEngine.AI;
using UnityEngine.EventSystems;

public class ClickToMove : MonoBehaviour 
{
    private PlayerBrain brain;

    [SerializeField]
    private LayerMask layerMask;

    private void Awake()
    {
        brain = GetComponent<PlayerBrain>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (EventSystem.current.IsPointerOverGameObject()) return;

            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, layerMask))
            {
                ITargetable targetable = hit.collider.GetComponent<ITargetable>();
                if (targetable != null)
                {
                    brain.TrySwitchState(new ChaseTargetable(brain, targetable));
                    TargetingCircle.Instance.SetTarget(targetable);
                }
                else
                {
                    brain.TrySwitchState(new MoveToPosition(brain, hit.point));
                    TargetingCircle.Instance.SetDestination(hit.point);
                }
            }
        }
    }
}
