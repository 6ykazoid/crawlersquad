﻿using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player Instance { get; private set; }

    public IAttackable Health { get; private set; }

    private void Awake()
    {
        Instance = this;
        Health = GetComponent<Health>();
    }
}
